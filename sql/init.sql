-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `znf-cviceni`;
CREATE DATABASE `znf-cviceni` /*!40100 DEFAULT CHARACTER SET utf16 COLLATE utf16_czech_ci */;
USE `znf-cviceni`;

DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf16_czech_ci NOT NULL,
  `phone` text COLLATE utf16_czech_ci NOT NULL,
  `registered` datetime NOT NULL,
  `is_dph` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;

INSERT INTO `company` (`id`, `name`, `phone`, `registered`, `is_dph`) VALUES
(1,	'Mrkvosoft',	'+42 123 243 242',	'2017-03-16 19:39:01',	1),
(2,	'Apple',	'234483002',	'2017-04-30 17:44:05',	1);

DROP TABLE IF EXISTS `employer`;
CREATE TABLE `employer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` text COLLATE utf16_czech_ci NOT NULL,
  `firstname` text COLLATE utf16_czech_ci NOT NULL,
  `salary` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `pid_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `company_ibfk_2` (`pid_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `company_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  CONSTRAINT `company_ibfk_2` FOREIGN KEY (`pid_id`) REFERENCES `pid` (`id`),
  CONSTRAINT `employer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_czech_ci;

INSERT INTO `employer` (`id`, `surname`, `firstname`, `salary`, `company_id`, `pid_id`, `user_id`) VALUES
(1,	'Gogo',	'Nafachcenko',	30000,	1,	2,	2),
(2,	'Karel',	'Breburda',	10000,	1,	1,	1),
(3,	'sdfsd',	'fffd',	23423,	2,	1,	NULL);

DROP TABLE IF EXISTS `pid`;
CREATE TABLE `pid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `pid` (`id`, `name`) VALUES
(1,	'7603164353'),
(2,	'7659191432'),
(6,	'23434');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `username`, `password`, `role`) VALUES
(1,	'delnik',	'delnik',	'delnik'),
(2,	'sef',	'sef',	'sef');

-- 2017-04-30 18:53:48
