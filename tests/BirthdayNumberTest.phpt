<?php

namespace Test;

use Nette;
use Tester;
use Tester\Assert;

$container = require __DIR__ . '/bootstrap.php';

class BirthdayNumberTest extends Tester\TestCase
{
    private $container;

    public function __construct(Nette\DI\Container $container)
    {
        $this->container = $container;
    }

    public function setUp()
    {
    }

    protected function getPids()
    {
        return [
            ['9010190046', true ],
            ['9010190045', false],
            ['9010190044', false],
            ['90101900463', false]
        ];
    }

    /**
     * @dataProvider getPids
     */
    public function testBirhdateNumber($a, $result)
    {
        $model = $this->container->getService('umodel');
        Assert::equal($model->isCorrectPid($a),$result);
    }

}
$test = new BirthdayNumberTest($container);
$test->run();
