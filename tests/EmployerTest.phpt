<?php

namespace Test;

use App\Model\EmployerModel;
use App\Model\NoDataFound;
use Nette;
use Tester;
use Tester\Assert;

$container = require __DIR__.'/bootstrap.php';

class EmployerTest extends Tester\TestCase
{
    private $container;

    public function __construct(Nette\DI\Container $container)
    {
        $this->container = $container;
    }

    public function setUp()
    {

    }

    public function createEmployer()
    {
        $employer = [
            'surname'    => 'Prijmeni',
            'firstname'  => 'Jmeno',
            'salary'     => 1290,
            'pid_id'     => null,
            'company_id' => 1,
        ];

        /** @var EmployerModel $model */
        $model = $this->container->getService('emodel');
        $employer['id']    = $model->insertEmployer($employer);


        return $employer;
    }

    public function testInsertion()
    {
        /** @var EmployerModel $model */
        $model = $this->container->getService('emodel');
        $employer = $this->createEmployer();

        $comparableEmployer = $model->getEmployer($employer['id']);
        Assert::equal($employer['surname'], $comparableEmployer['surname']);
        Assert::equal($employer['firstname'], $comparableEmployer['firstname']);
        Assert::equal($employer['salary'], $comparableEmployer['salary']);
        Assert::equal($employer['pid_id'], $comparableEmployer['pid_id']);
        Assert::equal($employer['company_id'], $comparableEmployer['company_id']);
    }

    public function testEdit()
    {
        /** @var EmployerModel $model */
        $model = $this->container->getService('emodel');
        $employer = $this->createEmployer();
        $id = $employer['id'];
        $employer['firstname'] = 'zmeneno';
        $model->updateEmployer($id, $employer);
        $employer = $model->getEmployer($id);

        Assert::equal($employer['firstname'], 'zmeneno');
    }
    public function testDelete()
    {
        /** @var EmployerModel $model */
        $model = $this->container->getService('emodel');
        $employer = $this->createEmployer();
        $id = $employer['id'];
        $model->deleteEmployer($id);
        Assert::exception(function() use ($model, $id){$model->getEmployer($id);}, NoDataFound::class, '');
    }
}

$test = new EmployerTest($container);
$test->run();
