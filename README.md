# BI-ZNF

## 6. Cvičení (11.5)

Varianta B projektu pro šesté cvičení předmětu BI-ZNF.

1. Použijte Vaši předchozí úlohu nebo si naklonujte a nainstalujte (příkaz "composer install") projekt 04b do vašeho lokálního adresáře.

2. Vytvořte si databázovou strukturu (MySQL) podle přítomného **SQL scriptu** a nastavte přístup k dané databázi v konfiguračním souboru ("app/config/config.local.neon")

3. Vytvořte jednotkový test pro otestování správné validace rodného čísla
  
4. Vytvořte jednotkový testy modelu pro vkládání, editaci a mazání zaměstnanců.

5. Nahraďte tabulky se seznamem zaměstnanců (Employer) libovolným rozšířením (např. Nextras/Datagrid)
  
6. Přidejte do libolného formuláře nový formulářový prvek (získaný jako rozšíření) (např. multichoice, catpcha)
