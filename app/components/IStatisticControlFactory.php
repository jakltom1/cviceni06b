<?php

namespace App\Components;

/**
 * @author Tomas
 */
interface IStatisticControlFactory
{
    /**
     * @return StatisticControl
     */
    public function create();
}