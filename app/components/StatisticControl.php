<?php
declare(strict_types=1);

namespace App\Components;

use App\Model\StatisticModel;
use Nette\Application\UI\Control;

/**
 * @author Tomas
 */
class StatisticControl extends Control
{
    /**
     * @var StatisticModel
     */
    private $statisticModel;

    /** @var  string */
    private $templateDir;

    /**
     * @param StatisticModel $statisticModel
     */
    public function __construct(StatisticModel $statisticModel)
    {
        $this->statisticModel = $statisticModel;
        $this->templateDir = __DIR__ . '/templates/statistic.latte';
    }

    public function render(int $employerID = 0)
    {
        $statistics = $this->statisticModel->listStatistic($employerID);
        $this->template->setFile($this->templateDir);
        $this->template->statistics = $statistics;
        $this->template->render();
    }
}