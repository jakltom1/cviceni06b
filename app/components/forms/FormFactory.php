<?php

namespace App\Forms;

use Nette\Application\UI\Form;
use Nette;

/**
 * @author Tomas
 */
class FormFactory
{
    use Nette\SmartObject;

    /**
     * @return Form
     */
    public function create() : Form
    {
        $form                                            = new Form;
        $renderer                                        = $form->getRenderer();
        $renderer->wrappers['controls']['container']     = null;
        $renderer->wrappers['pair']['container']         = 'div class="form-group"';
        $renderer->wrappers['pair']['.error']            = 'has-error';
        $renderer->wrappers['control']['container']      = '';
        $renderer->wrappers['label']['container']        = ' control-label text-right"';
        $renderer->wrappers['control']['description']    = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
        $renderer->wrappers['error']['container']        = null;
        $renderer->wrappers['error']['item']             = 'div class="alert alert-danger"';
        $form->getElementPrototype()->class('form-vertical ajax');
        $form->getElementPrototype()->setAttribute('novalidate', 'novalidate');

        $form->onRender[] = function ($form) {
            foreach ($form->getControls() as $control) {
                $type = $control->getOption('type');
                if ($type === 'button') {
                    $control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary btn-sm' : 'btn btn-default btn-sm');
                    $usedPrimary = true;
                } elseif (in_array($type, ['text', 'textarea', 'select'], true)) {
                    $control->getControlPrototype()->addClass('form-control input-sm');
                } elseif (in_array($type, ['checkbox', 'radio'], true)) {
                    $control->getSeparatorPrototype()->setName('div')->addClass($type.' input-sm');

                }
            }
        };

        return $form;
    }
}
