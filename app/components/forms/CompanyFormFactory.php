<?php

namespace App\Forms;

use App\Model\CompanyModel;
use Exception;
use Minetro\SeznamCaptcha\CaptchaValidator;
use Nette\Application\UI\Component;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Object;
use Nette\Utils\ArrayHash;


/**
 * Továrnička na tvorbu formulářů pro správu firem
 *
 * @author     Jiří Chludil
 * @author     Jindřich Máca
 * @author     Tomas Jakl
 * @copyright  Copyright (c) 2017 Jiří Chludil
 * @package    App\Forms
 */
class CompanyFormFactory extends Component
{
    /** @var CompanyModel Model pro správu firem. */
    private $companyModel;

    /** @var  FormFactory */
    private $formFactory;

    /** @var  CaptchaValidator */
    private $captchaValidator;

    /**
     * Setter pro formulářovou továrničku a modely
     *
     * @param CompanyModel $companyModel
     * @param FormFactory  $formFactory
     */
    public function injectDependencies(CompanyModel $companyModel, FormFactory $formFactory)//, CaptchaValidator $validator)
    {
        $this->companyModel = $companyModel;
        $this->formFactory  = $formFactory;
//        $this->captchaValidator = $validator;
    }

    /** @inheritdoc */
    protected function addCommonFields(Container &$form, $args = null)
    {
        $form->addText('name', 'Název')
             ->setAttribute('placeholder', 'Vyplň jméno')
             ->setRequired('Je třeba vyplnit jméno');
        $form->addText('phone', 'Telefon')
             ->setAttribute('placeholder', 'Vyplň telefon')
             ->setRequired('Je třeba vyplnit telefon');
        $form->addCheckbox('is_dph', 'Plátce DPH?');

        $form->addCaptcha('captcha');

    }

    /**
     * Vytváří komponentu formuláře pro přidávání nové firmy.
     *
     * @param null|array $args další argumenty
     *
     * @return Form formulář pro přidávání nové firmy
     */
    public function createAddForm($args = null)
    {
        $form = $this->formFactory->create(null, 'addForm');
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('send', 'Přidej');
        $form->onSuccess[] = [$this, "newFormSucceeded"];

        return $form;
    }

    /**
     * Vytváří komponentu formuláře pro editaci firmy.
     *
     * @param null|array $args další argumenty
     *
     * @return Form formulář pro editaci firmy
     */
    public function createEditForm($args = null)
    {
        $form = $this->formFactory->create(null, 'editForm');
        $form->addProtection('Ochrana');
        $this->addCommonFields($form);
        $form->addSubmit('send', 'Aktualizuj');
        $form->addHidden('id');
        $form->onSuccess[] = [$this, "editFormSucceeded"];

        return $form;
    }

    /**
     * Vytváří komponentu formuláře pro smazání firmy.
     *
     * @param null|array $args další argumenty
     *
     * @return Form formulář pro smazání firmy
     */
    public function createDeleteForm($args = null)
    {
        $form = $this->formFactory->create(null, 'deleteForm');
        $form->addProtection('Ochrana');
        $form->addSubmit('send', 'Odeber');
        $form->addHidden('id');
        $form->onSuccess[] = [$this, "deleteFormSucceeded"];

        return $form;
    }

    /**
     * Zpracování validních dat z formuláře a následného přidání firmy.
     *
     * @param Form      $form   formulář
     * @param ArrayHash $values data
     */
    public function newFormSucceeded(Form $form, ArrayHash $values)
    {
        try {
            if ( $form['captcha']->verify() === false){
                $form->addError('Captcha neni validni.');
            }else{
                $this->companyModel->insertCompany($values);
            }
        } catch (Exception $exception) {
            $form->addError($exception);
        }
    }

    /**
     * Zpracování validních dat z formuláře a následné aktualizace firmy
     *
     * @param Form      $form   formulář
     * @param ArrayHash $values data
     */
    public function editFormSucceeded(Form $form, ArrayHash $values)
    {
        try {

            if ( $form['captcha']->verify() === false){
                $form->addError('Captcha neni validni.');
            }else {
                $id = $values['id'];
                unset($values['id']);
                unset($values['captcha']);
                $this->companyModel->updateCompany($id, $values);
            }
        } catch (Exception $exception) {
            $form->addError($exception);
        }
    }

    /**
     * Zpracování validních dat z formuláře a následného odebrání firmy.
     *
     * @param Form      $form   formulář
     * @param ArrayHash $values data
     */
    public function deleteFormSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $this->companyModel->deleteCompany($values['id']);
        } catch (Exception $exception) {
            $form->addError($exception);
        }
    }

}
