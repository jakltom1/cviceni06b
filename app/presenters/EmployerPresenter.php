<?php

namespace App\Presenters;

use App\Components\IStatisticControlFactory;
use App\Forms\EmployerFormFactory;
use App\Model\CompanyModel;
use App\Model\EmployerModel;
use App\Model\NoDataFound;
use App\Model\PidModel;
use App\Model\UtilityModel;
use Nette\Application\UI\Form;
use Ublaboo\DataGrid\DataGrid;


class EmployerPresenter extends BasePresenter
{
    /** @var EmployerFormFactory - Formulářová továrnička pro správu zaměstanců */
    private $formFactory;

    /** @var EmployerModel - model pro management zaměstanců */
    private $employerModel;

    /** @var CompanyModel - model pro management firem */
    private $companyModel;

    /** @var UtilityModel - model pro management rc */
    private $utilityModel;

    /** @var PidModel - model pro management rc */
    private $pidModel;

    /** @var  IStatisticControlFactory */
    private $statisticControlFactory;

    /**
     * Setter pro formulářovou továrničku a modely správy uživatelů
     *
     * @param EmployerFormFactory      $formFactory   automaticky injectovaná formulářová továrnička
     * @param EmployerModel            $employerModel automatiky injetovaný model
     * @param CompanyModel             $companyModel  automatiky injetovaný model pro správu uživatelů
     * @param UtilityModel             $utilityModel  automatiky injetovaný model
     * @param PidModel                 $pidModel      automatiky injetovaný model
     * @param IStatisticControlFactory $statisticControlFactory
     */
    public function injectDependencies(
        EmployerFormFactory $formFactory,
        EmployerModel $employerModel,
        CompanyModel $companyModel,
        UtilityModel $utilityModel,
        PidModel $pidModel,
        IStatisticControlFactory $statisticControlFactory
    )
    {
        $this->formFactory             = $formFactory;
        $this->employerModel           = $employerModel;
        $this->companyModel            = $companyModel;
        $this->utilityModel            = $utilityModel;
        $this->pidModel                = $pidModel;
        $this->statisticControlFactory = $statisticControlFactory;
    }

    public function createComponentSimpleGrid($name)
    {
        $grid      = new DataGrid($this, $name);
        $employers = $this->employerModel->listEmployers($this->user);

        $empl = [];
        foreach ($employers as &$employer) {
            $pid            = $employer->pid === null ? 0 : $employer->pid->name;
            $company        = $employer->company;
            $arr            = $employer->toArray();
            $arr['pid']     = $pid;
            $arr['company'] = $company;

            $ret = $this->utilityModel->isMan(0, $pid);
            if ($ret === 1) {
                $arr['gender'] = 'muz';
            } else if ($ret === -1) {
                $arr['gender'] = 'nelze zjistit';
            } else {
                $arr['gender'] = 'zena';
            }


            $birthday        = $this->utilityModel->getBirthDay($employer['pid_id']);
            $arr['birthday'] = $birthday === -1 ? 'Neni uvedeno' : $birthday;
            $arr['pid']      = $arr['pid'] === 0 ? '!!' : $arr['pid'];
            $fee             = $this->employerModel->calculateFee($arr['salary']);
            $arr['fee']      = $fee;
            $empl[]          = $arr;

        }

        $grid->setDataSource($empl);
        $grid->addColumnText('company', 'Firma');
        $grid->addColumnText('firstname', 'Jmeno');
        $grid->addColumnText('surname', 'Prijmeni');
        $grid->addColumnText('pid', 'Rodne cislo');
        $grid->addColumnText('gender', 'Pohlavi');
        $grid->addColumnText('birthday', 'Datum narozeni');
        $grid->addColumnText('salary', 'Plat');
        $grid->addColumnText('fee', 'Dan');

        $grid->addAction('detail', 'Detail', 'Detail')
             ->setClass('btn btn-xs btn-primary');

        $grid->addAction('edit', 'Edit', 'Edit')
             ->setClass('btn btn-xs btn-warning');

        $grid->addAction('delete', 'Delete', 'Delete')
             ->setClass('btn btn-xs btn-danger');
        //        <a n:if="$user->isAllowed('Employer','detail')" class="btn btn-xs btn-primary" href="{plink detail id => $e->id}">Detail</a>
        //                            <a n:if="$user->isAllowed('Employer', 'edit') && $e->user_id == $user->getId()"
        //                                    class="btn btn-xs btn-warning"
        //                                    href="{plink edit id => $e->id}">
        //                                         Edituj
        //                            </a>
        //                            <a n:if="$user->isAllowed('Employer', 'delete') && $e->user_id == $user->getId()"
        //                                    class="btn btn-xs btn-danger"
        //                                    href="{plink delete id => $e->id}">
        //                                         Odeber
        //                            </a>
    }

    /**
     * Akce pro vkádání
     */
    public function actionAdd()
    {
        $form = $this['addForm'];
        try {
            $companies = $this->companyModel->listCompanies();
            $c         = [];
            foreach ($companies as $company) {
                $c[$company['id']] = $company['name'];
            }
            $form['company_id']->setItems($c);

            $pids = $this->pidModel->listPids();
            $p    = [0 => '==========='];
            foreach ($pids as $pid) {
                $p[$pid['id']] = $pid['name'];
            }
            $form['pid_id']->setItems($p);

        } catch (NoDataFound $e) {
            $form->addError('Nelze načíst data');
        }
    }

    /**
     * Akce pro editaci
     *
     * @param int $id id zaměstnance
     */
    public function actionEdit($id)
    {
        $form = $this['editForm'];
        try {
            $companies = $this->companyModel->listCompanies();
            $c         = [];
            foreach ($companies as $company) {
                $c[$company['id']] = $company['name'];
            }
            $form['company_id']->setItems($c);

            $pids = $this->pidModel->listPids();
            $p    = [0 => '==========='];
            foreach ($pids as $pid) {
                $p[$pid['id']] = $pid['name'];
            }
            $form['pid_id']->setItems($p);

            $employer = $this->employerModel->getEmployer($id);
            $form->setDefaults($employer);
        } catch (NoDataFound $e) {
            $form->addError('Nelze načíst data');
        }
    }

    /**
     * Akce pro mazání
     *
     * @param int $id id zaměstnance
     */
    public function actionDelete($id)
    {
        $form = $this['deleteForm'];
        $form['id']->setDefaultValue($id);
    }

    public function onFormError()
    {
        if ($this->isAjax()) {
            $this->redrawControl('employer');
        } else {
            $this->redirect($this);
        }
    }

    /**
     * Metoda pro vytvoření formuláře pro vložení
     *
     * @return Form - formulář
     */
    public function createComponentAddForm()
    {
        $form              = $this->formFactory->createAddForm();
        $form->onError[]   = [$this, 'onFormError'];
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };

        return $form;
    }

    public function createComponentStatistic()
    {
        return $this->statisticControlFactory->create();
    }

    /**
     * Metoda pro vytvoření formuáře pro editaci
     *
     * @return Form - formulář
     */
    public function createComponentEditForm()
    {
        $form              = $this->formFactory->createEditForm();
        $form->onError[]   = [$this, 'onFormError'];
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };

        return $form;
    }

    /**
     * Metoda pro vytvoření formuláře pro mazání
     *
     * @return Form - formulář
     */
    public function createComponentDeleteForm()
    {
        $form              = $this->formFactory->createDeleteForm();
        $form->onSuccess[] = function (Form $form) {
            $this->redirect('Employer:default');
        };

        return $form;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderEdit($id)
    {
        $employer             = $this->employerModel->getEmployer($id);
        $this->template->name = $employer['firstname'].' '.$employer['surname'];
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDetail($id)
    {
        $employer                   = $this->employerModel->getEmployer($id);
        $this->template->name       = $employer['firstname'].' '.$employer['surname'];
        $this->template->employerID = $employer['id'];
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDelete($id)
    {
        $employer             = $this->employerModel->getEmployer($id);
        $this->template->name = $employer['firstname'].' '.$employer['surname'];
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDefault()
    {
        $this->template->employers     = $this->employerModel->listEmployers($this->getUser());
        $this->template->utility       = $this->utilityModel;
        $this->template->employerModel = $this->employerModel;
    }

    public function handleCalculateFee($salary)
    {
        $this->employerModel->calculateFee($salary);
        if (!$this->isAjax()) {
            $this->redirect('this');
        }

        //        return $this->employerModel->calculateFee($salary);
    }
}
