<?php

namespace App\Presenters;

use Nette\Application\ForbiddenRequestException;
use Nette\Application\UI\MethodReflection;
use Nette\Application\UI\Presenter;
use Nette\Security\IUserStorage;

/**
 * Class BasePresenter
 *
 * @package App\Presenters
 */
abstract class BasePresenter extends Presenter
{
    public $basePath = __DIR__;

    /**
     * Check authorization
     *
     * @param $element
     *
     * @return void
     * @throws ForbiddenRequestException
     */
    public function checkRequirements($element)
    {
        if (!$this->getUser()->isLoggedIn()) {
            if ($this->getUser()->getLogoutReason() === IUserStorage::INACTIVITY) {
                $this->flashMessage('You have been signed out due to inactivity. Please sign in again.');
            }
            $this->redirect('Sign:in', ['backlink' => $this->storeRequest()]);
        } else {
            if ($element instanceof \ReflectionMethod) {
                $presenter = basename(str_replace('\\', '/', get_class($this)));
                $presenter = preg_replace( "/Presenter$/", "", $presenter);

                $method = $element->getName();
                $method = strtolower(preg_replace( "/^action/", "", $method));
                $method = preg_replace( "/^render/", "", $method);
                $method = preg_replace( "/^handle/", "", $method);

                foreach ($this->user->getRoles() as $role) {
                    if ($this->user->getAuthorizator()->isAllowed($role, $presenter, $method)) {
                        return;
                    }
                }
                throw new ForbiddenRequestException();
            } else {
                return;
            }
        }
    }
}
