<?php

namespace App\Presenters;

use App\Components\IStatisticControlFactory;
use App\Components\StatisticControl;
use App\Model\StatisticModel;


class StatisticPresenter extends BasePresenter
{

    /** @var StatisticModel - model pro statistiky */
    private $statisticModel;

    /** @var  IStatisticControlFactory */
    private $statisticControlFactory;

    /**
     * Setter pro model statistiky
     *
     * @param StatisticModel   $statisticModel automatiky injetovaný model pro správu statistik
     * @param StatisticControl $statisticControlFactory
     */
    public function injectDependencies(
        StatisticModel $statisticModel,
        IStatisticControlFactory $statisticControlFactory
    )
    {
        $this->statisticModel   = $statisticModel;
        $this->statisticControlFactory = $statisticControlFactory;
    }

    public function createComponentStatistic()
    {
        return $this->statisticControlFactory->create();
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDefault()
    {
        $this->template->statistics = $this->statisticModel->listStatistic();
    }
}
