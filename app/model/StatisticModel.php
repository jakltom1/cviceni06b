<?php

namespace App\Model;


use App\Components\Exceptions\NoResultFound;

class StatisticModel extends BaseModel
{

    /** @var CompanyModel - model pro management firem */
    private $companyModel;

    /** @var EmployerModel - model pro management zaměstanců */
    private $employerModel;

    public function __construct(CompanyModel $companyModel, EmployerModel $employerModel)
    {
        $this->companyModel  = $companyModel;
        $this->employerModel = $employerModel;
    }

    /**
     * Metoda vrací seznam všech statistik firem, záznam bude mít položky název firmz,
     * minální plat ve firmě, maximální plat ve firmě, průměrný plat a součet všech platů.
     *
     * @param int $employerID
     *
     * @return array
     */
    public function listStatistic(int $employerID = 0)
    {
        /** TODO */
        //return  $this->database->table('employer')->order('surname ASC')->fetchAll();

        try {
            $companies[] = $this->companyModel->getCompanyForEmployer($employerID);
        } catch (NoResultFound $e) {
            $companies = $this->companyModel->listCompanies();
        }

        $statistic = [];
        foreach ($companies as $c) {
            $employees = $this->employerModel->getEmployeesByCompanyWithSum($c->id);
            if ($employees->count("id") == 0) {
                $statistic[] = [
                    'name' => $c->name,
                    'min'  => 0,
                    'max'  => 0,
                    'avg'  => 0,
                    'sum'  => 0,
                ];
            } else {
                $min = $employees->min("salary");
                $max = $employees->max("salary");
                $sum = $employees->sum("salary");
                $avg = $sum / $employees->count("id");

                $statistic[] = [
                    'name' => $c->name,
                    'min'  => $min,
                    'max'  => $max,
                    'avg'  => $avg,
                    'sum'  => $sum,
                ];
            }
        }

        return $statistic;
    }
}